var hamburger = document.querySelector(".hamburger");
var drop = document.getElementById("drop");
var bool = true;

hamburger.addEventListener("click", function () {
    hamburger.classList.toggle("is-active");
    if(bool){
        drop.classList.toggle('active');
        bool = false;
    }else{
        drop.classList.toggle('active');
        bool = true;
    }
});