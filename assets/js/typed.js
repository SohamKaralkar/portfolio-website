var options = {
    strings: ['Developer', 'Designer', "Web Developer",'Debugger', 'Photographer'],
    typeSpeed: 40,
    backSpeed: 40,
    loop: true
};

var typed = new Typed('.typed-element', options);
